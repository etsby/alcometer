import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  genders = [];
  time: number;
  weight: number;
  gender: string;
  bottles: number;
  promilles: number;
  grams: number;



  constructor() {}

  ngOnInit() {
    this.genders.push('Female');
    this.genders.push('Male');  

    this.gender = 'Male';
    this.gender = 'Female';
   
  }

  calculate() {

    

    const litres = this.bottles * 0.33;
    const grams = litres * 8 * 4.5; 

    const burning = this.weight / 10; 

    grams - (burning * this.time);

    const grams_left =  grams - (burning * this.time) ;


    if (this.gender === 'Male') {
      this.promilles =  grams_left / (this.weight * 0.7) ;
    } else {
      this.promilles = grams_left / (this.weight * 0.6) ;
    }

  }

}
